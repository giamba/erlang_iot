-define(PROTOCOL_VERSION,           16#F9).

-define(START_SYSEX,                16#F0).
-define(QUERY_FIRMAWARE,            16#79).
-define(END_SYSEX,                  16#F7).

-define(CAPABILITY_QUERY,           16#6B).
-define(CAPABILITY_RESPONSE,        16#6C).

-define(ANALOG_MAPPING,             16#69).
-define(ANALOG_MAPPING_RESPONSE,    16#6A).

-define(SET_PIN_VALUE,              16#F5).


-define(SET_PIN_MODE,               16#F4).
-define(TOGLLE_ANALOG_IN_REPORTING, 16#C0).
-define(PIN_REPORTIN_ON,            16#01).
-define(PIN_REPORTIN_OFF,           16#00).

%% Pins
-define(PIN_0, 16#00).
-define(PIN_1, 16#01).
-define(PIN_2, 16#02).
-define(PIN_3, 16#03).
%% ... 127

%%Pin mode
%%mode (INPUT/OUTPUT/ANALOG/PWM/SERVO/I2C/ONEWIRE/STEPPER/ENCODER/SERIAL/PULLUP, 0/1/2/3/4/6/7/8/9/10/11)
-define(PIN_MODE_DIGITAL_INPUT,   16#00).
-define(PIN_MODE_DIGITAL_OUTPUT,  16#01).
-define(PIN_MODE_ANALOG_INPUT,    16#02).
-define(PIN_MODE_PWM,             16#03).
-define(PIN_MODE_SERVO,           16#04).
-define(PIN_MODE_I2C,             16#05).
-define(PIN_MODE_ONEWIRE,         16#06).
-define(PIN_MODE_STEPPER,         16#07).
-define(PIN_MODE_ENCODER,         16#08).
-define(PIN_MODE_SERIAL,          16#09).
-define(PIN_MODE_PULLUP,          16#0a).
%%
