-module(iot_blink).
%% API
-export([test/0,clientBlink/0]).


test() ->
  io:format("- blink test... ~n").

clientBlink() ->
  SomeHostInNet = "192.168.1.148",
  {ok, Sock} = gen_tcp:connect(SomeHostInNet, 3030, [binary, {packet, 0}]),

  gen_tcp:send(Sock, [16#f9]),              %Protocol version
  gen_tcp:send(Sock, [16#f0,16#79,16#f7]),  %Query Firmware Name and Version
  gen_tcp:send(Sock, [16#f0,16#6b,16#f7]),  %Capability Query
  gen_tcp:send(Sock, [16#f0,16#69,16#f7]),  %Analog Mapping Query
  gen_tcp:send(Sock, [16#f4,16#0d,16#03]),  %Set pin mode
  timer:sleep(2000),

  
  gen_tcp:send(Sock, [16#ed,16#7f,16#01]),  %Set pin on
  timer:sleep(1000),
  gen_tcp:send(Sock, [16#ed,16#00,16#00]),  %Set pin off
  timer:sleep(1000),
  gen_tcp:send(Sock, [16#ed,16#7f,16#01]),
  timer:sleep(1000),
  gen_tcp:send(Sock, [16#ed,16#00,16#00]),
  timer:sleep(1000),
  gen_tcp:send(Sock, [16#ed,16#7f,16#01]),
  timer:sleep(1000),
  gen_tcp:send(Sock, [16#ed,16#00,16#00]),



  ok = gen_tcp:close(Sock).






















