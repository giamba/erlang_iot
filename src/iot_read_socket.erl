-module(iot_read_socket).


%% API
-export([test/0, clientBlink/0, read_socket/1]).


test() ->
  io:format("read_socket test... ~n").

clientBlink() ->
  SomeHostInNet = "192.168.1.58",
  
  {ok, Sock} = gen_tcp:connect(SomeHostInNet, 3030, [binary, {packet, 0},{active, false}]),

  spawn(fun() -> read_socket(Sock) end),

  gen_tcp:send(Sock, [16#f9]),              %Protocol version
  gen_tcp:send(Sock, [16#f0,16#79,16#f7]),  %Query Firmware Name and Version
  gen_tcp:send(Sock, [16#f0,16#6b,16#f7]),  %Capability Query
  gen_tcp:send(Sock, [16#f0,16#69,16#f7]),  %Analog Mapping Query
  gen_tcp:send(Sock, [16#f4,16#0d,16#03]),  %Set pin mode
  timer:sleep(2000),

  gen_tcp:send(Sock, [16#ed,16#7f,16#01]),  %Set pin on
  timer:sleep(1000),
  gen_tcp:send(Sock, [16#ed,16#00,16#00]),  %Set pin off
  timer:sleep(1000),
  gen_tcp:send(Sock, [16#ed,16#7f,16#01]),
  timer:sleep(1000),
  gen_tcp:send(Sock, [16#ed,16#00,16#00]),
  timer:sleep(1000),
  gen_tcp:send(Sock, [16#ed,16#7f,16#01]),
  timer:sleep(1000),
  gen_tcp:send(Sock, [16#ed,16#00,16#00]),
  
  timer:sleep(10000),

  ok = gen_tcp:close(Sock).



read_socket(Sock) ->
  {ok, A} = gen_tcp:recv(Sock, 1),
  io:format("BinToList: ~p ", binary_to_list(A)),
  io:format("Raw: ~p ~n", [A]),
  read_socket(Sock).


