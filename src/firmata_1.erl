-module(firmata_1).

-include("firmata.hrl").


%% API
-export([ device/2,
          close/1,
          protocol_version/1,
          query_firmware/1,
          capability_query/1,
          analog_mapping/1,
          setPinMode/2,
          setDigitalPin/3]).


-spec device(string(), integer()) -> gen_tcp:socket().

device(Ip, Port) ->
  {ok, Socket} = gen_tcp:connect(Ip, Port, [binary, {packet, 0}, {active, false}]),  %%passive mode => need to call recv
  Socket.

close(Socket) ->
  gen_tcp:close(Socket).

-spec protocol_version(gen_tcp:socket()) -> binary().

protocol_version(Socket) ->
  ok = gen_tcp:send(Socket, [?PROTOCOL_VERSION]),
  {ok, <<_ProtocolVersion:1/binary, MajorVersion:1/binary, MinorVersion:1/binary>>} = gen_tcp:recv(Socket, 3),
  list_to_binary([MajorVersion, MinorVersion]).


-spec query_firmware(gen_tcp:socket()) -> binary().

query_firmware(Socket) ->
  ok = gen_tcp:send(Socket, [?START_SYSEX, ?QUERY_FIRMAWARE, ?END_SYSEX]),
  Data = read_response(Socket, <<>>),
  BodySize = byte_size(Data)-5,

  <<_StartSysex:1/binary, _QueryFirmware:1/binary, _MajorVer:1/binary, _MinorVer:1/binary,
    Body:BodySize/binary,
    _EndSysex:1/binary>> = Data,

  to_binary_list(Body).


-spec capability_query(gen_tcp:socket()) -> [binary()].

capability_query(Socket) ->
  ok = gen_tcp:send(Socket, [?START_SYSEX, ?CAPABILITY_QUERY, ?END_SYSEX]),
  Data = read_response(Socket, <<>>),
  BodySize = byte_size(Data)-3,
  <<_StartSysex:1/binary, _CapabilityResponse:1/binary,
    Body:BodySize/binary,
    _EndSysex:1/binary>> = Data,
  to_binary_list(Body).


-spec analog_mapping(gen_tcp:socket()) -> [binary()].

analog_mapping(Socket) ->
  ok = gen_tcp:send(Socket, [?START_SYSEX, ?ANALOG_MAPPING, ?END_SYSEX]),
  Data = read_response(Socket, <<>>),
  BodySize = byte_size(Data)-3,
  <<_StartSysex:1/binary, _AnalogMappingResponse:1/binary,
    Body:BodySize/binary,
    _EndSysex:1/binary>> = Data,
  to_binary_list(Body).


setPinMode(Socket, PinNumber) ->
  ok = gen_tcp:send(Socket, [?SET_PIN_MODE, PinNumber, ?PIN_MODE_DIGITAL_OUTPUT]).


setDigitalPin(Socket, PinNumber, Value) ->
  ok = gen_tcp:send(Socket, [?SET_PIN_VALUE, PinNumber, Value]).



readAnalog(Socket, PinNumber) ->
  {}.



-spec readData(gen_tcp:socket(), list()) -> binary().

readData(Socket, List) ->
  {ok, Data} = gen_tcp:recv(Socket, 1),
  %%io:format("-> Data: ~p~n",[Data]),
  case Data of
    <<?END_SYSEX>>  ->
      lists:reverse(List);
    _Any ->
      readData(Socket, [Data|List])
  end.

read_response(Socket, Bin) ->
  {ok, <<Data/binary>>} = gen_tcp:recv(Socket, 1),
  case Data of
    <<?END_SYSEX>> ->
      <<Bin/binary, ?END_SYSEX>>;
    _Any ->
      read_response(Socket, <<Bin/binary, Data/binary>>)
  end.


to_binary_list(Bin) ->
  [X || <<X:1/binary>> <= Bin].