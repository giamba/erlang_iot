-module(test_firmata).

-include("firmata.hrl").

%% API
-export([test/0]).


test() ->
    Dev = firmata:device("192.168.1.58", 3030),

    ProtocolVersion = firmata_1:protocol_version(Dev),
    io:format("-> Protocol Version: ~p ~n", [ProtocolVersion]),

    Firmware = firmata_1:query_firmware(Dev),
    io:format("-> Query Firmaware: ~p~n",[Firmware]),
    %io:format("-> Query Firmaware: ~s~n",[Firmware]),

    Capability  = firmata_1:capability_query(Dev),
    io:format("-> Capability Query: ~p~n", [Capability]),

    AnalogMapping  = firmata_1:analog_mapping(Dev),
    io:format("-> Analog Mapping: ~p~n",[AnalogMapping]),

    %set pin mode
    %firmata_1:setPinMode(Dev, 13, ?PIN_MODE_DIGITAL_OUTPUT),

    %set led on
    %firmata_1:setDigitalPin(Dev, 13, 1),

    %timer:sleep(2000),

    %set led off
    %firmata_1:setDigitalPin(Dev, 13, 0),

    %timer:sleep(2000),
    

    %set pin mode
    %firmata_1:setPinMode(Dev, 0, ?PIN_MODE_ANALOG_INPUT),   %%f4:00:02


    %firmata_1:readAnalog(Dev, 0),

    


    firmata:close(Dev).