-module(iot_process_1).

%% API
-export([main/0, spawn_device/1, create_device/1]).

main() ->
  BlinkObj = spawn_device("192.168.1.58"),
  BlinkObj ! on,
  timer:sleep(1000),
  BlinkObj ! off,
  timer:sleep(1000),
  BlinkObj ! on,
  timer:sleep(1000),
  BlinkObj ! off,
  timer:sleep(1000).

spawn_device(IpAddress) ->
  SocketBlink = init_device(IpAddress),
  spawn(?MODULE, create_device, [SocketBlink]).

 init_device(IpAddress) ->
   {ok, Sock} = gen_tcp:connect(IpAddress, 3030, [binary, {packet, 0}, {active, false}]),
   gen_tcp:send(Sock, [16#f9]),              %Protocol version
   gen_tcp:send(Sock, [16#f0,16#79,16#f7]),  %Query Firmware Name and Version
   gen_tcp:send(Sock, [16#f0,16#6b,16#f7]),  %Capability Query
   gen_tcp:send(Sock, [16#f0,16#69,16#f7]),  %Analog Mapping Query
   gen_tcp:send(Sock, [16#f4,16#0d,16#03]),  %Set pin mode
   timer:sleep(2000),
   Sock.

create_device(Sock) ->
  receive
    on ->
      gen_tcp:send(Sock, [16#ed,16#7f,16#01]);  %Set pin On
    off ->
      gen_tcp:send(Sock, [16#ed,16#00,16#00])   %Set pin Off
  end,
  create_device(Sock).

















