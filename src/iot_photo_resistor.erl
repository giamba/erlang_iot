-module(iot_photo_resistor).

-include("firmata.hrl").

%% API
-export([test/0, photo_resistor/0]).

test() ->
  io:format("photo resistor test... ~n").


photo_resistor() ->
  SomeHostInNet = "192.168.1.131",
  {ok, Sock} = gen_tcp:connect(SomeHostInNet, 3030, [binary, {packet, 0},{active, false}]),

  gen_tcp:send(Sock, [?PROTOCOL_VERSION]),           
  {ok, _Package} = gen_tcp:recv(Sock, 3),

  Ris = readVal(Sock, 10),
  io:format("-> Average: ~p ~n", [average(Ris)]),
  
  io:format("End: ~p ~n", [ok]).



readVal(Sock, NumOfSample) ->
  gen_tcp:send(Sock,[?TOGLLE_ANALOG_IN_REPORTING, ?PIN_REPORTIN_ON]), % set reporting on
  List = readAnalog(Sock, NumOfSample, []) ,
  gen_tcp:send(Sock,[?TOGLLE_ANALOG_IN_REPORTING, ?PIN_REPORTIN_OFF]), % set reporting off
  ok = flush_socket(Sock),
  [calcValue(X) || X <- List].

calcValue(Binary) ->

  List = binary_to_list(Binary),
  io:format("--------> : ~p ~n", [List]),
  First = lists:nth(2, List),
  Second = lists:nth(3, List),
  First bor (Second bsl 7).

readAnalog(_Sock, 0, List) ->
  List;
readAnalog(Sock, N, List) ->
  {ok, Package} = gen_tcp:recv(Sock, 3),
  L = lists:append([Package], List),
  readAnalog(Sock, N-1, L).

flush_socket(Sock) ->
   case gen_tcp:recv(Sock, 3, 700) of
     {error,timeout} ->
       ok;
     _Any ->
       flush_socket(Sock)
   end.

average(List) ->
  lists:sum(List) / length(List).




