-module(firmata).

-include("firmata.hrl").

-define(SOCKET_TIMEOUT_MILLI_SEC, 500).

%% API
-export([ device/2,
          close/1,
          protocol_version/1,
          query_firmware/1,
          capability_query/1,
          analog_mapping/1,
          setPinMode/3,
          setDigitalPin/3]).

-spec device(string(), integer()) -> gen_tcp:socket().

device(Ip, Port) ->
  {ok, Socket} = gen_tcp:connect(Ip, Port, [binary, {packet, 0}, {active, false}]),  %%passive mode => need to call recv
  Socket.

close(Socket) ->
  gen_tcp:close(Socket).

-spec protocol_version(gen_tcp:socket()) -> [binary()].

protocol_version(Socket) ->
  gen_tcp:send(Socket, [?PROTOCOL_VERSION]),
  [ X || X <- lists:reverse(readData(Socket, [])),
    binary_to_list(X) =/= [?PROTOCOL_VERSION]].


-spec query_firmware(gen_tcp:socket()) -> [binary()].

query_firmware(Socket) ->
  gen_tcp:send(Socket, [?START_SYSEX, ?QUERY_FIRMAWARE, ?END_SYSEX]),
  [ X || X <- lists:reverse(readData(Socket, [])),
    binary_to_list(X) =/= [?START_SYSEX],
    binary_to_list(X) =/= [?QUERY_FIRMAWARE],
    binary_to_list(X) =/= [?END_SYSEX]].


-spec capability_query(gen_tcp:socket()) ->  [binary()].

capability_query(Socket) ->
gen_tcp:send(Socket, [?START_SYSEX, ?CAPABILITY_QUERY, ?END_SYSEX]),
  [ X || X <- lists:reverse(readData(Socket, [])),
    binary_to_list(X) =/= [?START_SYSEX],
    binary_to_list(X) =/= [?CAPABILITY_RESPONSE],
    binary_to_list(X) =/= [?END_SYSEX]].


-spec analog_mapping(gen_tcp:socket()) ->  [binary()].

analog_mapping(Socket) ->
  gen_tcp:send(Socket, [?START_SYSEX, ?ANALOG_MAPPING, ?END_SYSEX]),
  [ X || X <- lists:reverse(readData(Socket, [])),
    binary_to_list(X) =/= [?START_SYSEX],
    binary_to_list(X) =/= [?ANALOG_MAPPING_RESPONSE],
    binary_to_list(X) =/= [?END_SYSEX]].


setPinMode(Socket, PinNumber, PinMode) ->
  ok = gen_tcp:send(Socket, [?SET_PIN_MODE, PinNumber, PinMode]).


setDigitalPin(Socket, PinNumber, Value) ->
  ok = gen_tcp:send(Socket, [?SET_PIN_VALUE, PinNumber, Value]).
  

-spec readData(gen_tcp:socket(), list()) -> list().

readData(Socket, List) ->
  case gen_tcp:recv(Socket, 1, ?SOCKET_TIMEOUT_MILLI_SEC) of   %% set to 0 to receive all bytes
    {error, timeout} ->
      List;
    {ok, Data} ->
         readData(Socket, lists:append([Data], List))
  end.

