-module(iot_servo).

%% API
-export([test/0, servo/0]).

test() ->
  io:format("servo test... ~n").


servo() ->
  SomeHostInNet = "192.168.1.50",
  {ok, Sock} = gen_tcp:connect(SomeHostInNet, 3030, [binary, {packet, 0}]),

  gen_tcp:send(Sock, [16#f9]),              %Protocol version
  gen_tcp:send(Sock, [16#f0,16#79,16#f7]),  %Query Firmware Name and Version
  gen_tcp:send(Sock, [16#f0,16#6b,16#f7]),  %Capability Query
  gen_tcp:send(Sock, [16#f0,16#69,16#f7]),  %Analog Mapping Query
  gen_tcp:send(Sock, [16#f4,16#0e,16#04]),  %Set Pin 14 as Servo
  
  gen_tcp:send(Sock,[16#f4,16#0e,16#04]),
  gen_tcp:send(Sock,[16#f4,16#0e,16#04]),
  
  timer:sleep(2000),

  gen_tcp:send(Sock,[16#ee,16#00,16#00]),   %%min

  timer:sleep(1000),

  gen_tcp:send(Sock,[16#ee,16#5a,16#00]), %% 90 deg

  timer:sleep(1000),

  gen_tcp:send(Sock,[16#ee,16#34,16#01]),   %%max

  ok = gen_tcp:close(Sock).
