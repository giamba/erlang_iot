var net = require('net');
var five = require('johnny-five');
var firmata = require('firmata');

var options = {
  host: '192.168.1.10',  //whatever host
  port: 3030  //some port
};

var client = net.connect(options, function() { //'connect' listener
  console.log('connected to server!');
  var socketClient = this;
  //we can use the socketClient instead of a serial port as our transport
  var io = new firmata.Board(socketClient);

  io.once('ready', function(){
    console.log('io ready');

    var board = new five.Board({io: io, repl: true});

    board.on('ready', function(){
        console.log('five ready');

        var servo = new five.Servo(14);

        // Servo alternate constructor with options
        var servo = new five.Servo({
            id: "MyServo",     // User defined id
            pin: 14,           // Which pin is it attached to?
            //type: "standard",  // Default: "standard". Use "continuous" for continuous rotation servos
            //range: [0,180],    // Default: 0-180
            //fps: 100,          // Used to calculate rate of movement between positions
            //invert: false,     // Invert all specified positions
            //startAt: 0,       // Immediately move to a degree
            //center: true,      // overrides startAt if true and moves the servo to the center of the range
        });

        /*this.wait(3000, function() {
             servo.min();
          });
          */

        /*this.wait(5000, function() {
             servo.max();
          });
         */

        this.wait(5000, function() {
            servo.to( 90 );
         });
          
     
        //servo.sweep();
     }); //on
  }); //once
});

