var net = require('net');
var five = require('johnny-five');
var firmata = require('firmata');

var options = {
  host: '192.168.1.10',  //whatever host
  port: 3030  //some port
};

var client = net.connect(options, function() { //'connect' listener
  console.log('connected to server!');

  var socketClient = this;
  //we can use the socketClient instead of a serial port as our transport
  var io = new firmata.Board(socketClient);

  io.once('ready', function(){
    console.log('io ready');
    io.isReady = true;

    var board = new five.Board({io: io, repl: true});

    board.on('ready', function(){
      console.log('five ready');

      var led = new five.Led(13); //13 --> pin 7

      // "blink" the led in 500ms on-off phase periods
      led.blink(1000);

    });



  });
});